DOCUMENTACIÓN EJERCICIO 4 DEL TEMA 1

Calculadora:

Calculadora es una aplicación para android que emula una calculadora básica la cual consta de los siguientes elementos gráficos:

	Botones

		Números

			Son botones que van desde el 0 al 9. En el caso del 0, si se pulsa por primera vez, controla que sólo puede haber un cero por pantalla.

		Operadores

			+	:	Indica que se va a operar una suma.
			-	:	Indica que se va a operar una resta.
			*	:	Indica que se va a operar una multiplicación.
			/	:	Indica que se va a operar una división.
			=	:	Permite calcular las operaciones ya indicadas.

		Botones especiales

			·	:	El punto permite indicar que se va a escribir parte 
					decimal. Si se escribe por primera vez la parte entera es un cero seguido del punto y el resto de decimales.

			C	:	Con el botón Clear se reinician los valores de la 
					calculadora.

	Pantalla de salida

		Se trata de un TextView no mayor de 18 caracteres que muestra los operandos y las operaciones que se van a realizar.



Curiosidades:

	1. Si se escribe un número lo suficientemente grande como para contener más de 18 cifras, es decir, mayor que el TextView de salida, la aplicación muestra la operación en notación científica.

	2. Para comenzar a escribir con un número negativo, primero se pulsará el operador que permite la resta ( - ) y, a continuación, se deberá pulsar un número, el cual convertiremos en negativo pulsando el operador igual ( = ).

	3. Si se hace una división por cero, se mostrará un mensaje temporal de duración corta con la frase “No se puede dividir por cero”.

	4. Cuando se realizan operaciones con enteros, los resultados se muestran como tales, es decir, sin decimales; mientras que cuando se implica alguna operación con números reales, la operación se muestra como un número real con su parte decimal incluida.

	5. Para cambiar del operando1 al operando2, existe un booleano llamado op2Act, que cuando su valor es true activa el operando2 y lo desactiva cuando es false, cambiando así al operando1 cada vez que se pulse un operador. Para ello, utiliza la fórmula op2Act=!op2Act.

	6. El resultado de una operación anterior es la entrada del operando1 para las operaciones siguientes siempre y cuando no se haya pulsado el botón C. De esta forma, se pueden realizar diferentes operaciones básicas una detrás de otra en base a los resultados de las operaciones anteriores.

	7. Para el formateo de los String en Java se ha hecho uso de la ayuda de la documentación existente en Internet. No obstante, el método que los gestiona y los mecanismos utilizados son de invención propia.

